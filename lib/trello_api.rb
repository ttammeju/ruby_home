require 'uri'
require 'openssl'


class TrelloApi
	@@API_URL = "https://api.trello.com/1/"
	@@API_AUTH_PARAMS = "" #NOTE: so program will not crash, instead requests will produce "404 - Not Found"
	
	def TrelloApi.authenticateUser
		#no - ask creds, ask to save to .auth
		authFilePath = File.join(Dir.home, "trello.auth") # adds appropriate OS-specific seperator '/' vs '\'
		authFilePathExists = File.exist?(authFilePath.to_s)

		if authFilePathExists
			authLines = IO.readlines(authFilePath) #NOTE: 'readlines' method handles closing IO stream itself

			if authLines.length<2
				print "key: "
				userKey = gets.chomp
				print "token: "
				userToken = gets.chomp
				
				
				puts "Save the credentials (y/n)"
				confirm = gets.chomp
				if confirm=="y"
					open(authFilePath, 'a') { |f|
						f.puts userKey
						f.puts userToken
					}
					@@API_AUTH_PARAMS = "?key=" + userKey + "&token=" + userToken
				else # we don't really care, as long as 'y' is handled
					raise RuntimeError, "Application needs authentication!"
				end
			else
				byKey = {"key" => 0, "token" => 1} #NOTE: represents the structure of the trello.auth file
				token = authLines[byKey["token"]].chomp
				key = authLines[byKey["key"]].chomp
				
				@@API_AUTH_PARAMS = "?key=" + key + "&token=" + token
			end
		elsif
		    File.open(authFilePath, "w") do |f|
                # Block will automatically close and flush the file stream
                f.write("no auth")
            end
            TrelloApi.authenticateUser #re-try to auth user
		end
	end
	
	def requestEndpoint(endpoint, method, params)
		uri = @@API_URL + endpoint + @@API_AUTH_PARAMS
		
		params.each do |key, value|
			uri = uri + "&" + key + "=" + value
		end
		puts "requesting: " + uri #TODO: delete in production
		uri = URI(uri)
		
		#Over HTTPS
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		
		response = http.send_request(method, uri)
		
		return response
	end
end