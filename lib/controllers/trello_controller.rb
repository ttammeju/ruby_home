require 'json'

class TrelloController
	def makeNewBoard(boardName)
		response = TrelloApi.new.requestEndpoint("boards", "POST", {"name" => boardName})
		
		case response
			when Net::HTTPSuccess, Net::HTTPRedirection
				puts "Board '" + boardName + "' was created sucessfully!"
			else
				response.value
		end
	end
	
	def makeNewList(listName, boardId)
		response = TrelloApi.new.requestEndpoint("list", "POST", {"idBoard" => boardId, "name" => listName})
		
		case response
			when Net::HTTPSuccess, Net::HTTPRedirection
				puts "List '" + listName + "' was created sucessfully!"
			else
				response.value
		end
	end
	
	#GET /1/members/[idMember or username]/boards; NOTE: Board must be public, to access without a token
	#PUT /1/boards/[board_id]/prefs/permissionLevel
	def getAllBoards
		response = TrelloApi.new.requestEndpoint("members/ruby_home/boards", "GET", {"fields" => "name"})
		
		case response
			when Net::HTTPSuccess, Net::HTTPRedirection
				return JSON.parse(response.body)
			else
				response.value
		end
	end
end