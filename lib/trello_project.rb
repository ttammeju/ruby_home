require_relative "trello_project/version"

  require 'net/http'
  require_relative 'trello_api'
  require_relative 'controllers/trello_controller'

module TrelloProject
  #disposable email: ruby_home_project@yopmail.com

  class ConsoleProject
  	# dev api key: 92ccae2e17ee3bd7e36fca3e9d4b4c1c #TODO: delete in production
  	# Oauth: a096d55148ec3396a51d2706afc3378efb7d1c932525115d749abdfb6f001d3b - for local testing https://trello.com/1/token/approve #TODO: delete in production
  	@@commands = ["new list", "new board", "all boards"]

  	def start
  		@api = TrelloController.new
  		TrelloApi.authenticateUser

  		prompt = "> " # prompt character
  		print prompt

  		while user_input = gets.chomp # loop while getting user input
  			case user_input
  				when @@commands[0]
  					print "name: "
  					newListName = gets.chomp
  					print "id: "
  					boardId = gets.chomp
  					@api.makeNewList(newListName, boardId)
  					print prompt
  				when @@commands[1]
  					print "name: "
  					newBoardName = gets.chomp
  					@api.makeNewBoard(newBoardName)
  					print prompt
  				when @@commands[2]
  					@boards = @api.getAllBoards
  					@boards.each do |inp|
  						puts inp["name"] + " (" + inp["id"] + ")"
  					end
  					print prompt
  				when "help"
  					printAvailableCmds
  					print prompt
  				else
  					puts "see: 'help'"
  					print prompt
  					next
  			end
  		end
  	end


  	def printAvailableCmds
  		puts "Available commands:"
  		@@commands.each { |x| puts "\t '" + x + "'\n" }
  	end

  end


  console = ConsoleProject.new
  console.start




end
