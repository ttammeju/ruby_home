# TrelloProject

Project that implements some Trello's functionality. So far implemented:

* creation of boards

* creation of lists

* displaying all boards


But it has potential to implement all Trello's REST API's endpoints - the base is there.

Pretty straightforward console-application. There are some available commands to use, which you can see by typing 'help'.



## Installation

Add this line to your application's Gemfile:

```ruby
gem 'trello_project'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install trello_project


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/ttammeju/ruby_home. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).